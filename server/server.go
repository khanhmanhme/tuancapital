package server

import (
	"log"
	"start/config"
	"start/datasources"
	"start/socket"
)

func Init() {
	log.Println("server init")

	// ============ Use mysql
	datasources.MySqlConnect(false)
	MigrateDb(false)
	// ============ Use redis
	datasources.MyRedisConnect()
	//Init Socket Server
	go socket.Init()
	//
	config := config.GetConfig()
	r := NewRouter()
	log.Println("Server is running ...", "listen", config.GetString("backend_port"))
	r.Run(config.GetString("backend_port"))

}
