package middlewares

import (
	"github.com/gin-gonic/gin"
	"log"
	"start/config"
	"start/datasources"
	"start/myjwt"
	"start/response_message"
	"start/utils/constants"
	"encoding/json"
)

func VerifyJwtToken() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.Request.Header.Get(constants.JWT_USER_TOKEN)
		config := config.GetConfig()
		dataClaim := myjwt.DataClaims{}
		dataClaim, checkJwt, err := myjwt.VerifyJwtToken(token, config.GetString("jwt_secret"))
		if dataClaim.Status != constants.STATUS_ENABLE {
			c.JSON(401, response_message.GetErrorResponseData(c.Request.Header.Get(constants.HEADER_KEY_LANGUAGE), "USER_DISABLED", "User is disable"))
			c.Abort()
			return
		}

		// check cache
		jwtStore, errCache := datasources.GetCache(dataClaim.Uid)
		if errCache != nil {
			log.Println("Error cache: ", errCache)
			c.JSON(401, response_message.GetErrorResponseData(c.Request.Header.Get(constants.HEADER_KEY_LANGUAGE), "JWT_TOKEN_INVALID", "Cache return nil"))
			c.Abort()
			return
		}
		if jwtStore != token {
			c.JSON(401, response_message.GetErrorResponseData(c.Request.Header.Get(constants.HEADER_KEY_LANGUAGE), "JWT_TOKEN_INVALID", "jwtStore != token"))
			c.Abort()
			return
		}

		b, err := json.Marshal(dataClaim)
		if err != nil {
			log.Println("Error marshal: ", err)
			c.JSON(401, response_message.GetErrorResponseData(c.Request.Header.Get(constants.HEADER_KEY_LANGUAGE), "JWT_TOKEN_INVALID", err.Error()))
			// c.JSON(500, gin.H{"message": "Parse json data failed"})
			c.Abort()
			return
		}

		if checkJwt == false {
			log.Println("Error check jwt: ", err)
			c.JSON(401, response_message.GetErrorResponseData(c.Request.Header.Get(constants.HEADER_KEY_LANGUAGE), "JWT_TOKEN_INVALID", err.Error()))
			c.Abort()
			return
		} else {
			c.Set(constants.KEY_CONTEXT_USER, b)
			c.Next()
		}
	}
}
