package response_message

var DefaultLanguage = map[string]string{
	// COMMON
	"SUCCESS":                    "Success",
	"SYSTEM_ERROR":               "System error",
	"CANNOT_REGISTER":            "Cannot register",
	"ERROR_REQUEST_DATA":         "Error request data",
	"ERROR_VALIDATE_DATA":        "Data is invalidate",
	"ERROR_NOT_FOUND":            "Record not found",
	"ERROR_NOT_FIND_USER":        "Cannot find user",
	"ERROR_UPDATE_USER":          "Cannot update user",
	"ERROR_CREATE":               "Lỗi khởi tạo chuyến đi",
	"ERROR_SEND_EMAIL":           "Cannot send email",
	"ERROR_UPDATE_PROFILE":       "Cannot update profile",
	"EXPIRE_TIME_VALIDATE_EMAIL": "Time to validate email is expired",
	"ERROR_VALIDATE_EMAIL":       "Link verify email is wrong or expired",

	"ERROR_USER_TYPE": "Đối tượng không được phục vụ",

	// ADMIN
	"VALIDATE_EMAIL_PASSWORD": "Email or pass invalidate",
	"EMAIL_EXIST":             "Email has been registed",
	"ERROR_FIND_ADMIN":        "Cannot find admin",

	// USER
	"USER_DISABLED":                "Tài khoản bạn đã bị khóa, xin vui lòng liên lạc với tổng đài",
	"PLEASE_WAIT_2S":               "Vui lòng đợi 2 giây để đặt cuốc mới!",
	"JWT_TOKEN_INVALID":            "Đăng nhập không thành công",
	"VALIDATE_SOURCE_INVALID":      "Start point is not available",
	"VALIDATE_DESTINATION_INVALID": "Destination point is not available",
	"VALIDATE_INVALID_DISTANCE":    "Chi phuc vu trong ban kinh 30 km",
	"INVALID_PICKING":              "Điểm chọn của bạn không nằm trong khu vực phục vụ!",

	"LIMIT_AVATAR_UPLOAD": "Dung lượng ảnh tối đa 300kb",
	"ERROR_UPDATE_AVATAR": "Lỗi cập nhật Avatar",

	// Payment
	"ERROR_FIND_PAYMENT": "Không tìm thấy phương thức thanh toán",

	// Permission
	"ERROR_FIND_PERMISSION": "Cannot find permission",

	// Booking
	"HAVE_A_BOOKING":          "Cuốc đi của bạn vẫn chưa kết thúc",
	"ERROR_BOOKING":           "Cuốc xe của bạn bị lỗi",
	"ERROR_FIND_BOOKING":      "Không tìm thấy cuốc đặt",
	"ERROR_FINISH_BOOKING":    "Không thể kết thúc chuyến đi",
	"ERROR_CANCEL_BOOKING":    "Cannot cancel booking",
	"ERROR_BOOKING_STATUS":    "Booking status is not available",
	"BOOKING_RATED":           "Cuốc đặt đã được đánh giá",
	"LIMIT_TIME_RATE_BOOKING": "Quá thời gian đánh giá cuốc đã đặt",

	// Pricing
	"ERROR_FIND_PRICE": "Cannot find trip price",

	// Service
	"ERROR_FIND_SERVICE": "Cannot find service",

	// Favorites
	"ERROR_FIND_FAVORITE": "Cannot find favorite",

	// Notification
	"ERROR_FIND_NOTIFICATION": "Cannot find notification",

	// out partner
	"ERROR_FIND_PARTNER": "Partner or secret is not match",
}
