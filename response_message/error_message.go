package response_message

import "start/config"

type ResponseMessage struct {
	Language string `json:"language"`
	Message  string `json:"message"`
}

type ErrorResponseData struct {
	Message string `json:"message"`
	Log     string `json:"log"`
}

var languages = map[string]map[string]string{
	"default": DefaultLanguage,
	"vi":      ViLanguage,
}

func (item *ResponseMessage) SetLanguage(language string) {
	// keys := []string{}
	for k := range languages {
		if language == k {
			item.Language = k
			return
		}
	}
	item.Language = "default"
}

func (item *ResponseMessage) GetMessage(key string) string {
	if item.Language == "" {
		item.Language = "default"
	}
	item.Message = languages[item.Language][key]
	return item.Message
}

func GetMessageByLanguage(language, key string) string {
	if language == "" {
		language = "default"
	}
	return languages[language][key]
}

func GetErrorResponseData(language, key, log string) ErrorResponseData {
	lstLanguage := make([]string, 0, len(languages))
	checkLanguageExist := false
	for _, item := range lstLanguage {
		if item == language {
			checkLanguageExist = true
		}
	}
	if checkLanguageExist == false {
		language = "default"
	}

	errData := ErrorResponseData{
		Message: languages[language][key],
		Log:     log,
	}
	config := config.GetConfig()
	if config.GetBool("system_log_response") == false {
		errData.Log = ""
	}
	return errData
}
