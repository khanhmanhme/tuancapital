package controllers

import (
	"github.com/gin-gonic/gin"
	"log"
	"start/common/models"
	"start/config"
	"start/datasources"
	"start/myjwt"
	"start/response_message"
	"start/utils/constants"
	"start/utils/validation"
	"strconv"
)

type CustomerController struct {

}

func (cuser *CustomerController) GetProfile(c *gin.Context) {
}

func (cuser *CustomerController) UserRegister(c *gin.Context) {
	phone := c.PostForm("phone")
	password := c.PostForm("password")

	validateOk, errValidate := validation.Validate(validation.RegisterForm{Phone: phone, Password: password})
	if validateOk == false {
		c.JSON(400, response_message.GetErrorResponseData(c.Request.Header.Get(constants.HEADER_KEY_LANGUAGE), "ERROR_VALIDATE_DATA", errValidate.Error()))
		c.Abort()
		return
	}

	user := models.Customer{}
	// check user exist
	err := user.FindCustomerByPhone(phone)
	if err == nil {
		c.JSON(400, response_message.GetErrorResponseData(c.Request.Header.Get(constants.HEADER_KEY_LANGUAGE), "EMAIL_EXIST", "Phone has been registed"))
		c.Abort()
		return
	}

	newUser := new(models.Customer)
	check, err := newUser.CreateCustomer(phone, password)

	if check == false {
		c.JSON(500, response_message.GetErrorResponseData(c.Request.Header.Get(constants.HEADER_KEY_LANGUAGE), "SYSTEM_ERROR", err.Error()))
		c.Abort()
		return
	}

	c.JSON(200, gin.H{"message": "Register success"})
	c.Abort()
	return
}


func (cuser *CustomerController) Login(c *gin.Context) {
	phone := c.PostForm("phone")
	password := c.PostForm("password")
	ttl := c.PostForm("ttl")

	var ttlInt int
	if ttl == "" {
		ttlInt = constants.JWT_EXPIRE_TIME
	} else {
		var errConvert error
		ttlInt, errConvert = strconv.Atoi(ttl)
		if errConvert != nil {
			ttlInt = constants.JWT_EXPIRE_TIME
			log.Println("Error", errConvert)
		}
	}

	validateOk, errValidate := validation.Validate(validation.RegisterForm{Phone: phone, Password: password})
	if validateOk == false {
		log.Println("Error", errValidate)
		c.JSON(500, response_message.GetErrorResponseData(c.Request.Header.Get(constants.HEADER_KEY_LANGUAGE), "ERROR_REQUEST_DATA", errValidate.Error()))
		c.Abort()
		return
	}

	// check user exist
	user := models.Customer{}
	err := user.FindCustomerByPhone(phone)
	if err != nil {
		log.Println(err)
		c.JSON(500, response_message.GetErrorResponseData(c.Request.Header.Get(constants.HEADER_KEY_LANGUAGE), "SYSTEM_ERROR", err.Error()))
		c.Abort()
		return
	}

	checkPassword := models.CheckPassword(password, user.Password)

	if checkPassword == false {
		c.JSON(400, response_message.GetErrorResponseData(c.Request.Header.Get(constants.HEADER_KEY_LANGUAGE), "VALIDATE_PHONE_PASSWORD", ""))
		c.Abort()
		return
	}

	dataClaim := myjwt.DataClaims{
		Uid:      user.Uid,
		Phone:    user.Phone,
		Password: user.Password,
		Status:   user.Model.Status,
	}
	config := config.GetConfig()
	jwtToken, err := myjwt.CreateJwtToken(dataClaim, config.GetString("jwt_secret"), ttlInt)
	if err != nil {
		log.Println("Error", err)
		c.JSON(500, response_message.GetErrorResponseData(c.Request.Header.Get(constants.HEADER_KEY_LANGUAGE), "SYSTEM_ERROR", err.Error()))
		c.Abort()
		return
	}

	errCache := datasources.SetCache(user.Uid, jwtToken, ttlInt)
	if errCache != nil {
		log.Println("Error", errCache)
		c.JSON(400, response_message.GetErrorResponseData(c.Request.Header.Get(constants.HEADER_KEY_LANGUAGE), "SYSTEM_ERROR", errCache.Error()))
		c.Abort()
		return
	}

	c.JSON(200, gin.H{"token": jwtToken})
	c.Abort()
	return
}

