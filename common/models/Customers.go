package models

import (
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"log"
	"start/datasources"
	"start/utils/constants"
	"time"
)

type Customer struct {
	Model
	Phone string `json:"phone"`
	Password string `json:"password"`
	Balance float64 `json:"balance"`
}

func (user *Customer) FindCustomerByPhone(phone string) error {
	mydb := datasources.GetDatabase()
	res := mydb.Find(user, &Customer{Phone: phone})
	return res.Error
}

func (user *Customer) CreateCustomer(phone, password string) (bool, error) {
	uid := uuid.New()
	createdAt := time.Now().Unix()
	updatedAt := time.Now().Unix()

	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return false, err
	}

	createUser := Customer{
		Model: Model{
			Uid:       uid.String(),
			CreatedAt: createdAt,
			UpdatedAt: updatedAt,
			Status:    constants.STATUS_ENABLE,
		},
		Phone:    phone,
		Password: string(hash),
	}
	mydb := datasources.GetDatabase()
	errCreate := mydb.Create(&createUser).Error
	if errCreate != nil {
		return false, errCreate
	}
	return true, nil
}

func CheckPassword(password, hashPassword string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashPassword), []byte(password))
	if err != nil {
		log.Println("Error", err)
		return false
	}
	return true

}