package models

type Model struct {
	Uid       string `gorm:"primary_key" sql:"not null;" json:"uid"`
	CreatedAt int64  `json:"created_at"`
	UpdatedAt int64  `json:"upadted_at"`
	Status    string `sql:"size:50" json:"status"` //ENABLE, DISABLE, TESTING
}