package constants

var JWT_EXPIRE_TIME = 86400 * 30 // unit: seconds
var KEY_CONTEXT_USER = "KEY_CONTEXT_USER"

var JWT_USER_TOKEN = "authorization"
var JWT_ADMIN_TOKEN = "token-admin"
var HEADER_KEY_LANGUAGE = "language"

var STATUS_ENABLE = "ENABLE"
var STATUS_DISABLE = "DISABLE"