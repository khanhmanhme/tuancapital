package validation

import (
	"github.com/asaskevich/govalidator"
	"log"
)

type RegisterForm struct {
	Phone    string `valid:"alphanum,required"`
	Password string `valid:"alphanum,required"`
}

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

func Validate(register RegisterForm) (bool, error) {
	ok, err := govalidator.ValidateStruct(register)
	return ok, err
}