package socket

import (
	"flag"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"strconv"
	"time"
)

var addr = flag.String("addr", "localhost:8000", "http service address")
var upgrader = websocket.Upgrader{} // use default options
var maxtimeperround = 30

func echo(w http.ResponseWriter, r *http.Request) {
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer c.Close()
	for {
		//mt, message, err := c.ReadMessage()
		//if err != nil {
		//	log.Println("read:", err)
		//	break
		//}
		//log.Printf("recv: %s", message)
		HandleMatch(c)
	}
}


func Init() {
	flag.Parse()
	log.SetFlags(0)
	http.HandleFunc("/echo", echo)
	log.Fatal(http.ListenAndServe(*addr, nil))
}


func HandleMatch(conn *websocket.Conn){
	for{
		timestamp := strconv.FormatInt(time.Now().UTC().UnixNano(), 10)
		n := 0
		for n < maxtimeperround {
			time.Sleep(1 * time.Second)
			doSendNotification(timestamp,conn)
			n = n + 1
		}
	}
}

func doSendNotification(s string,conn *websocket.Conn) {
	err := conn.WriteMessage(1, []byte(s))
	if err != nil {
		//SOME CLIENT DISCONNECT , IGNORE !
	}
}
