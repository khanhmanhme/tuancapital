package myjwt

import (
	"github.com/dgrijalva/jwt-go"
	"time"
)

type DataClaims struct {
	Uid      string `json:"uid"`
	Phone    string `json:"phone"`
	Password string `json:"password"`
	Status   string `json:"status"`
	jwt.StandardClaims
}

func CreateJwtToken(data DataClaims, secretKey string, duration int) (string, error) {
	dataClaim := DataClaims{
		data.Uid,
		data.Phone,
		data.Password,
		data.Status,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Second * time.Duration(duration)).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), dataClaim)
	tokenstring, err := token.SignedString([]byte(secretKey))

	return tokenstring, err
}

func VerifyJwtToken(tokenString, secretKey string) (DataClaims, bool, error) {
	data := DataClaims{}
	token, err := jwt.ParseWithClaims(tokenString, &data, func(token *jwt.Token) (interface{}, error) {
		return []byte(secretKey), nil
	})

	checkToken := false
	if err == nil && token.Valid {
		checkToken = true
	}

	return data, checkToken, err
}
